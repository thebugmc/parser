package net.thebugmc.parser.pattern;

import net.thebugmc.parser.Parser;

/**
 * Represents a result of a sentence parse.
 *
 * @param length Amount of pieces used by this pattern.
 * @param result Resulting sentence.
 * @param <T> Sentence type, matching {@link ParsePattern} and {@link Parser}.
 */
public record PatternResult<T extends Sentence>(int length, T result) {
    /**
     * Check if resulting sentence is a statement.
     *
     * @return {@link Sentence#isStatement()}
     */
    public boolean isStatement() {
        return result.isStatement();
    }
}
