package net.thebugmc.parser.group;

import net.thebugmc.parser.expression.ExpressionPiece;
import net.thebugmc.parser.expression.GroupPiece;

import java.util.List;

/**
 * Function that creates a group from the given boundaries.
 *
 * @param <L> Group opening piece type.
 * @param <R> Group closing piece type.
 */
@FunctionalInterface
public interface GroupSupplier<L extends net.thebugmc.parser.expression.ExpressionPiece, R extends net.thebugmc.parser.expression.ExpressionPiece> {
    /**
     * Create a group from the given pieces.
     *
     * @param left    Left boundary. Make sure it matches {@link Grouper#start(net.thebugmc.parser.expression.ExpressionPiece)}
     * @param right   Right boundary. Make sure it matches {@link Grouper#end(net.thebugmc.parser.expression.ExpressionPiece)}
     * @param content Pieces inside the group.
     * @return Group made from the pieces.
     */
    GroupPiece apply(L left, R right,
                     List<ExpressionPiece> content);
}
