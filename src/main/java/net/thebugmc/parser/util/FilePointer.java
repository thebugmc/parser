package net.thebugmc.parser.util;

import static net.thebugmc.parser.util.ParserException.ASSERT;

/**
 * An auxiliary structure of a line/column pair in a file.
 */
public record FilePointer(String fileName, int line, int column) {
    /**
     * Create a file pointer.
     *
     * @throws ParserException If line or column are not at least 1.
     */
    public FilePointer {
        ParserException.ASSERT(line > 0, "File pointer line should be at least 1");
        ParserException.ASSERT(column > 0, "File pointer column should be at least 1");
    }

    /**
     * Display file pointer to a <code>fileName line:column</code> format.
     *
     * @return <code>fileName line:column</code>
     */
    public String toString() {
        return fileName + " " + line + ":" + column;
    }
}
