package net.thebugmc.parser.util;

import net.thebugmc.parser.util.FilePointer;

/**
 * An object that has a file pointer associated with it.
 */
public class Pointable {
    private final net.thebugmc.parser.util.FilePointer ptr;

    /**
     * Create a Pointable from a given file position.
     *
     * @param ptr Creation position.
     */
    public Pointable(net.thebugmc.parser.util.FilePointer ptr) {
        this.ptr = ptr;
    }

    /**
     * Position in a file where this object was created.
     *
     * @return Creation position.
     */
    public FilePointer pointer() {
        return ptr;
    }
}
