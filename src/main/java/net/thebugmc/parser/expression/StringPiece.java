package net.thebugmc.parser.expression;

import net.thebugmc.parser.util.FilePointer;
import net.thebugmc.parser.util.ParserException;

import static java.lang.Character.digit;
import static net.thebugmc.parser.expression.PieceResult.CONTINUE;
import static net.thebugmc.parser.expression.PieceResult.TAKE;
import static net.thebugmc.parser.util.ParserException.ASSERT;

/**
 * Reads a string with escape sequences until it meets an end character (typically double or single quote).
 */
public class StringPiece extends ExpressionPiece {
    private final char end;
    private char start;

    private final StringBuilder sb = new StringBuilder();

    private boolean escape = false;
    private boolean started = false;

    private int uExpected = 0;
    private char uChar;

    /**
     * Construct a String Piece.
     *
     * @param ptr Creation pointer.
     * @param end Character that the string will end with. Does not have to be same as opening character.
     */
    public StringPiece(FilePointer ptr, char end) {
        super(ptr);
        this.end = end;
    }

    public PieceResult read(char c, FilePointer ptr) {
        if (uExpected > 0) {
            ASSERT(c >= '0' && c <= '9'
                || c >= 'a' && c <= 'f'
                || c >= 'A' && c <= 'F', ptr, "Expecting a \\u-sequence, \"" + escape(c) + "\" is not a hex digit");
            uChar |= digit(c, 16) << (--uExpected << 2);
            if (uExpected == 0) sb.append(uChar);
            return PieceResult.CONTINUE;
        }

        if (!started && (started = true)) {
            start = c;
            return PieceResult.CONTINUE;
        }
        ASSERT(c != '\n', ptr, "Cannot break lines mid-string");

        if (escape) {
            escape = false;
            if (c != end) {
                if (c == 'u') {
                    uChar = 0;
                    uExpected = 4;
                    return PieceResult.CONTINUE;
                }
                c = switch (c) {
                    case '/', '"', '\'', '`', '\\' -> c;
                    case 'n' -> '\n';
                    case 'r' -> '\r';
                    case 'f' -> '\f';
                    case 'b' -> '\b';
                    case 't' -> '\t';
                    case '0' -> '\0';
                    default -> throw new ParserException("Unexpected escape sequence \"\\" + c + "\"", ptr);
                };
            }
        } else if (c == '\\' && c != end) {
            escape = true;
            return PieceResult.CONTINUE;
        } else if (c == end) return PieceResult.TAKE;

        sb.append(c);
        return PieceResult.CONTINUE;
    }

    /**
     * Get content of the string stored in this piece.
     *
     * @return String value.
     */
    public String content() {
        return sb + "";
    }

    /**
     * Content wrapped with start and end characters.
     *
     * @return start + {@link #escape(CharSequence)} + end
     */
    public String toString() {
        return start + escape(sb) + end;
    }

    /**
     * Escape a character
     * (replace by an associated code with escape prefix, for example '\n' -> "\\n").
     *
     * @param c Character to escape.
     * @return Escaped character.
     */
    public static String escape(char c) {
        char[] ins = {'\\', '\"', '\'', '\n', '\r', '\f', '\b', '\t', '\0'};
        char[] out = {'\\', '\"', '\'', 'n', 'r', 'f', 'b', 't', '0'};

        // Find if any ins match character, replace by out
        for (int j = 0; j < ins.length; j++)
            if (c == ins[j]) return "\\" + out[j];

        return "" + c;
    }

    /**
     * Escape characters in the string.
     *
     * @param s String to escape.
     * @return Escaped string.
     */
    public static String escape(CharSequence s) {
        var res = new StringBuilder();
        for (int i = 0; i < s.length(); i++)
            res.append(escape(s.charAt(i)));
        return res + "";
    }
}
