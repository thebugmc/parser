package net.thebugmc.parser.expression;

import net.thebugmc.parser.expression.PieceResult;
import net.thebugmc.parser.util.FilePointer;

import static net.thebugmc.parser.expression.PieceResult.CONTINUE;
import static net.thebugmc.parser.expression.PieceResult.LEAVE;

/**
 * Reads letters, digits, and some predefined characters.
 */
public class NamePiece extends ExpressionPiece {
    private final String specialChars;
    private final StringBuilder sb = new StringBuilder();

    /**
     * Construct a name piece at a position.
     *
     * @param ptr Creation position.
     * @param specialChars Additional characters to read.
     */
    public NamePiece(FilePointer ptr, String specialChars) {
        super(ptr);
        this.specialChars = specialChars;
    }

    public PieceResult read(char c, FilePointer ptr) {
        if (!check(c, specialChars)) return LEAVE;
        sb.append(c);
        return CONTINUE;
    }

    /**
     * Get parsed value of the name piece.
     *
     * @return Name piece value.
     */
    public String name() {
        return sb + "";
    }

    /**
     * Get parsed value of the name piece.
     *
     * @return {@link #name()}
     */
    public String toString() {
        return name();
    }

    /**
     * Check whether a character can be read by a name piece with given special chars.
     *
     * @param c Character to check.
     * @param specialChars Additional characters to read.
     * @return True if character is a letter, digit, or in the specialChars supplied.
     */
    public static boolean check(char c, String specialChars) {
        return Character.isLetterOrDigit(c) || specialChars.indexOf(c) >= 0;
    }
}
