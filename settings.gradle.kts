rootProject.name = "parser"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenLocal()
    }
}